# KerArrow installation instructions

KerArrow is available at [this repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kerarrow).

## Requirements

The following tools are required to compile KerArrow:

- GNU Make 4.2.1
- g++ (GCC) 9.2.1
- CMake 3.11.1

Use the following commands to install CMake:

```bash
wget https://cmake.org/files/v3.11/cmake-3.11.1.tar.gz
tar -xvf cmake-3.11.1.tar.gz && mv cmake-3.11.1 cmake
cd cmake
```

Configure make and make it visible:

```bash
./configure --prefix=/path/to/cmake/
make
export PATH=/path/to/cmake/bin/:$PATH
```

## Source code

Clone the repository by using the following commands:

```bash
git clone git@gitlab.inria.fr:Kerdata/Kerdata-Codes/kerarrow.git
cd kerarrow
```

## Compiling

```bash
mkdir -p cpp/release && cd cpp/release/
cmake .. -DCMAKE_BUILD_TYPE=Release -DARROW_PLASMA=on
make -j4 && make install
```

Make sure to export the bin path:

```bash
export LD_LIBRARY_PATH="/path/to/kerarrow/cpp/release/release/:$LD_LIBRARY_PATH"
```

## Running Plasma

```bash
./path/to/kerarrow/cpp/release/release/plasma_store -m 4000000000 -s /tmp/plasma 1> /dev/null 2> /dev/null &
```