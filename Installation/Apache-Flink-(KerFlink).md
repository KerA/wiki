# KerFlink installation instructions

KerFlink is available at [this repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kerflink).

## Requirements

The following tools are required to compile KerFlink:

- Java 8
- Apache Maven >= 3.5.0

To get a working toolchain on Debian 10, this used to be sufficient:

```bash
# Maven
sudo apt install maven
```

## Source code

Clone the repository by using the following commands:

```bash
git clone git@gitlab.inria.fr:Kerdata/Kerdata-Codes/kerflink.git
cd kerflink
```

## KerFlink complete installation

KerA should already be [installed]() in `/opt/kera`. KerFlink should be installed in `/opt/kerflink`.

**1. Clone KerFlink**

Clone KerFlink:

```bash
cd /opt
git clone git@gitlab.inria.fr:Kerdata/Kerdata-Codes/kerflink.git
cd kerflink
```

**2. Link the KerA library**

Add the local KerA JAR into your local Maven repository as follows:

```bash
mvn install:install-file -Dfile=/opt/kera/bindings/java/build/install/kera/lib/ramcloud.jar -DgroupId=kera -DartifactId=ramcloud -Dversion=1 -Dpackaging=jar -DgeneratePom=true
```

If the JAR doesn't exist yet, generate it using the `java` rule provided by the KerA Makefile :

```bash
cd /opt/kera
make java
```

**3. Compile KerFlink**

Compile KerFlink:

```bash
mvn clean package -DskipTests -Dcheckstyle.skip=true
```

**4. Configure KerFlink**

Copy the KerA JAR to the libraries used by KerFlink:

```bash
cp /opt/kera/bindings/java/build/install/kera/lib/ramcloud.jar /opt/kerflink/build-target/lib/
```

Modify the following files in `/opt/kerflink/build-target/bin`:

```bash
flink 
flink-daemon.sh
taskmanager.sh

# Add the following line:
export LD_LIBRARY_PATH="/opt/kera/install/lib/kera/:/usr/local/include/:/opt/kera/kerarrow/cpp/release/release/"
```