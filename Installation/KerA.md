# KerA installation instructions

## Requirements

The following tools are required to compile KerA:

- GNU Make 4.2.1
- g++ (GCC) 9.2.1
- Python >= 3.7
- Perl (pcre) 8.43.3
- Boost 1.69
- Protocol Buffers (protoc) 3.6.1
- Java 8
- Zookeeper 3.4.9
- Apache Maven >= 3.5.0

To get a working toolchain on Fedora, this used to be sufficient:

```bash
# Dependencies
sudo dnf install git doxygen protobuf-devel boost-devel libevent-devel pcre-devel zookeeper-devel
```

To get a working toolchain on Debian 10, this used to be sufficient:

```bash
# Java
sudo apt-get update
sudo apt-get install -y wget gnupg software-properties-common
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
sudo apt-get update
sudo apt-get install -y adoptopenjdk-8-hotspot
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/adoptopenjdk-8-hotspot-amd64/bin/java 1500
# Dependencies
sudo apt-get install -y build-essential git doxygen libboost-all-dev libpcre3-dev protobuf-compiler libprotobuf-dev libcrypto++-dev libevent-dev libgtest-dev libzookeeper-mt-dev zookeeper libssl-dev libcppunit-dev libcppunit-doc
```

## Source code

Clone the repository by using the following commands:

```bash
git clone git@gitlab.inria.fr:Kerdata/Kerdata-Codes/kera.git
cd kera
git submodule update --init --recursive
```

## Compiling

```bash
make clean && make -j12
```

Object files and executables will appear in the subdirectory obj.<branch>, where <branch> is the current Git branch (typically master). By default, KerA compiles with debugging symbols, which slows the system down considerably. If you want to make performance measurements, recompile without debugging symbols:

```bash
make clean && make -j12 DEBUG=no
```

Multiple targets can be compiled:

- `make`
    - Build the KerA server and client software
    - Output `obj.<branch>/client`, `obj.<branch>/server`, `obj.<branch>/coordinator`
- `make test`
    - Build and run KerA unit tests
- `make check`
    - Run Google style checker against files in the src directory, subject to check as style evolves

## KerA complete installation

These instructions cover a complete installation of KerA combined with KerArrow and KerFlink.

KerA should be installed in `/opt/kera/kera`.

**1. Install KerA**

Clone KerA (do not forget to clone the submodules):

```bash
mkdir /opt/kera && cd /opt/kera
git clone git@gitlab.inria.fr:Kerdata/Kerdata-Codes/kera.git
cd kera
git submodule update --init --recursive
```

Compile KerA server:

```bash
make -j12
```

**2. Compile KerArrow**

```bash
cd /opt/kera
```

[Clone](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/wikis/Installation/KerArrow#source-code) and [compile](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/wikis/Installation/KerArrow#compiling) KerArrow.

**3. Test KerA consumers**

```bash
cd /opt/kera/kera
make test
```

Test pull and shared push consumer:

```bash
./bindings/java/gradlew -Dorg.gradle.daemon=true test --info --tests *KeraHandoverSharedPushClusterSetup.startKeraConsumerThread
```

**4. Install KerFlink**

If you want to use KerFlink zith KerA, please follow [these instructions](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/wikis/Installation/KerFlink/edit#kerflink-complete-installation).
